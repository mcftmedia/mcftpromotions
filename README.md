# McftPromotions

A Bukkit server plugin for Minecraft that allows for the easy promotion/demotion of users.

This was made to suit the needs of the [Diamond Mine](http://diamondmine.net) server.

**[License](https://diamondmine.net/plugins/license)** -- **[Issues](https://mcftmedia.atlassian.net/issues/?jql=project=PROMOTIONS)** --  **[Builds](https://mcftmedia.atlassian.net/builds/browse/PROMOTIONS-PROMOTIONS)**
-- **[Download](http://diamondmine.net/plugins/download/McftPromotions)** -- **[Bukkit Dev](http://dev.bukkit.org/server-mods/mcftpromotions/)**