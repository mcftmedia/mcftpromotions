package net.diamondmine.mcftpromotions;

import org.anjocaido.groupmanager.GroupManager;
import org.anjocaido.groupmanager.permissions.AnjoPermissionsHandler;
import org.bukkit.Bukkit;

import ru.tehkode.permissions.bukkit.PermissionsEx;

import com.platymuus.bukkit.permissions.PermissionsPlugin;

import de.bananaco.bpermissions.api.ApiLayer;
import de.bananaco.bpermissions.api.util.CalculableType;

/**
 * Handler for Permission plugins.
 * 
 * @author Jon la Cour
 * @version 1.0.9
 */
public class PermissionsHandler extends McftPromotions {
    /**
     * Get a player's group name.
     * 
     * @param world
     *            World name.
     * @param name
     *            Player name.
     * @return Group name or null.
     */
    public static String getGroup(final String world, final String name) {
        if (p.getName().equals("bPermissions")) {
            return ApiLayer.getGroups(world, CalculableType.USER, name)[0];
        } else if (p.getName().equals("GroupManager")) {
            GroupManager permission = (GroupManager) Bukkit.getServer().getPluginManager().getPlugin("GroupManager");
            AnjoPermissionsHandler handler = permission.getWorldsHolder().getWorldPermissionsByPlayerName(name);
            return handler.getGroup(name);
        } else if (p.getName().equals("PermissionsBukkit")) {
            PermissionsPlugin permission = (PermissionsPlugin) Bukkit.getServer().getPluginManager().getPlugin("PermissionsBukkit");
            if (permission.getPlayerInfo(name) == null) {
                return null;
            } else if (permission.getPlayerInfo(name).getGroups() != null && !permission.getPlayerInfo(name).getGroups().isEmpty()) {
                return permission.getPlayerInfo(name).getGroups().get(0).getName();
            }
        } else if (p.getName().equals("PermissionsEx")) {
            return PermissionsEx.getPermissionManager().getUser(name).getGroupsNames()[0];
        }
        return null;
    }

    /**
     * Get a player group prefix.
     * 
     * @param world
     *            World name.
     * @param name
     *            Player name.
     * @param group
     *            Group name.
     * @return Prefix or empty string.
     */
    public static String getPrefix(final String world, final String name, final String group) {
        if (p.getName().equals("bPermissions")) {
            return ApiLayer.getValue(world, CalculableType.USER, name, "prefix");
        } else if (p.getName().equals("GroupManager")) {
            GroupManager permission = (GroupManager) Bukkit.getServer().getPluginManager().getPlugin("GroupManager");
            AnjoPermissionsHandler handler = permission.getWorldsHolder().getWorldPermissionsByPlayerName(name);
            return handler.getGroupPrefix(group);
        } else if (p.getName().equals("PermissionsEx")) {
            return PermissionsEx.getPermissionManager().getGroup(group).getPrefix();
        }
        return "";
    }

    /**
     * Get a player group suffix.
     * 
     * @param world
     *            World name.
     * @param name
     *            Player name.
     * @param group
     *            Group name.
     * @return Suffix or empty string.
     */
    public static String getSuffix(final String world, final String name, final String group) {
        if (p.getName().equals("bPermissions")) {
            return ApiLayer.getValue(world, CalculableType.USER, name, "suffix");
        } else if (p.getName().equals("GroupManager")) {
            GroupManager permission = (GroupManager) Bukkit.getServer().getPluginManager().getPlugin("GroupManager");
            AnjoPermissionsHandler handler = permission.getWorldsHolder().getWorldPermissionsByPlayerName(name);
            return handler.getGroupSuffix(group);
        } else if (p.getName().equals("PermissionsEx")) {
            return PermissionsEx.getPermissionManager().getGroup(group).getSuffix();
        }
        return "";
    }
}
