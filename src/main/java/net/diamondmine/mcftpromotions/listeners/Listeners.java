package net.diamondmine.mcftpromotions.listeners;

import static net.diamondmine.mcftpromotions.Settings.commands;
import net.diamondmine.mcftpromotions.McftPromotions;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

/**
 * McftPromotions handler for custom commands. TODO: Rewrite for 1.1.0.
 * 
 * @author Jon la Cour
 * @version 1.0.7
 */
public class Listeners implements Listener {
    @SuppressWarnings("unused")
    private static McftPromotions plugin;

    /**
     * Construct.
     * 
     * @param instance
     *            McftPromotions JavaPlugin.
     */
    public Listeners(final McftPromotions instance) {
        plugin = instance;
    }

    /**
     * Pre-command processing.
     * 
     * @param event
     *            PlayerCommandPreprocessEvent
     */
    @EventHandler(priority = EventPriority.NORMAL)
    public final void onPlayerCommandPreprocess(final PlayerCommandPreprocessEvent event) {
        String[] args = event.getMessage().split(" ", 2);
        if (args.length > 1) {
            String cmd = args[0];
            for (String group : commands.keySet()) {
                if (cmd.equalsIgnoreCase("/" + group)) {
                    event.setCancelled(true);
                    Player player = event.getPlayer();
                    if (player != null) {
                        player.chat("/changegroup " + group + " " + args[1]);
                    }
                    break;
                }
            }
        }
    }
}
