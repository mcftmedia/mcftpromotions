package net.diamondmine.mcftpromotions;

import static net.diamondmine.mcftpromotions.Settings.checkSettings;
import static net.diamondmine.mcftpromotions.Settings.commands;
import static net.diamondmine.mcftpromotions.Settings.grammar;
import static net.diamondmine.mcftpromotions.Settings.groups;
import static net.diamondmine.mcftpromotions.Settings.loadSettings;
import static net.diamondmine.mcftpromotions.Settings.settings;

import java.util.List;
import java.util.logging.Logger;

import net.diamondmine.mcftprofiler.McftProfiler;
import net.diamondmine.mcftprofiler.Notes;
import net.diamondmine.mcftpromotions.listeners.Listeners;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Core of McftPromotions.
 * 
 * @author Jon la Cour
 * @version 1.0.7
 */
public class McftPromotions extends JavaPlugin {
    public static final Logger logger = Logger.getLogger("Minecraft");
    private static McftProfiler profiler = null;
    public static Permission p;

    /**
     * Plugin disabled.
     */
    @Override
    public final void onDisable() {
        log("Version " + getDescription().getVersion() + " is disabled!");
    }

    /**
     * Plugin enabled.
     */
    @Override
    public final void onEnable() {
        // Permissions
        if (!setupPermissions()) {
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        // Settings
        checkSettings();
        loadSettings();

        // Register events
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new Listeners(this), this);

        // McftProfiler check
        if (pluginExists(new String[] {"net.diamondmine.mcftprofiler.McftProfiler"})) {
            Plugin mcftProfiler = getServer().getPluginManager().getPlugin("McftProfiler");
            if (mcftProfiler.isEnabled()) {
                profiler = (McftProfiler) mcftProfiler;
            }
        }

        PluginDescriptionFile pdfFile = getDescription();
        String version = pdfFile.getVersion();
        log("Version " + version + " enabled", "info");
    }

    @Override
    public final boolean onCommand(final CommandSender sender, final Command cmd, final String commandLabel, String[] args) {
        String cmdname = cmd.getName().toLowerCase();
        Player player = null;
        String pname = ChatColor.DARK_RED + "[Console]";
        boolean console = true;
        if (sender instanceof Player) {
            player = (Player) sender;
            pname = player.getName();
            console = false;
        }

        for (String command : commands.keySet()) {
            if (cmdname.equalsIgnoreCase(command)) {
                args = unshift(cmdname, args);
                cmdname = "changegroup";
            }
        }

        if (cmdname.equalsIgnoreCase("checkrank")) {
            String puser = args[0];
            if (getServer().getPlayer(puser) != null) {
                Player user = getServer().getPlayer(puser);
                puser = user.getName();
            }
            if (console || (player != null && p.has(player, "mcftpromotions.checkrank"))) {
                if (args.length == 1) {
                    String worldname = null;
                    if (console) {
                        List<World> worldlist = getServer().getWorlds();
                        worldname = worldlist.get(0).getName();
                    } else {
                        worldname = player.getLocation().getWorld().getName();
                    }
                    String group = PermissionsHandler.getGroup(worldname, puser);
                    if (!group.isEmpty() && group != null) {
                        String prefix = fixColor(PermissionsHandler.getPrefix(worldname, puser, group));
                        String suffix = fixColor(PermissionsHandler.getSuffix(worldname, puser, group));
                        String rank = prefix + group + suffix;
                        sender.sendMessage(ChatColor.GOLD + puser + ": " + rank);
                    } else {
                        sender.sendMessage(ChatColor.GOLD + puser + ": No rank.");
                    }
                } else {
                    sender.sendMessage(ChatColor.GOLD + "Please enter a valid username!");
                }
            }
        }

        if (cmdname.equalsIgnoreCase("changegroup")) {
            if (args.length >= 2) {
                for (String command : commands.keySet()) {
                    if (args[0].equalsIgnoreCase(command)) {
                        String puser = args[1];
                        String newgroup = commands.get(command);
                        Player user;
                        if (getServer().getPlayer(puser) != null) {
                            user = getServer().getPlayer(puser);
                            puser = user.getName();
                        }
                        if (console || (player != null && p.has(player, "mcftpromotions." + command))) {
                            String worldname = null;
                            if (console) {
                                List<World> worldlist = getServer().getWorlds();
                                worldname = worldlist.get(0).getName();
                            } else {
                                worldname = player.getLocation().getWorld().getName();
                            }
                            if (!console && (puser.equals(pname) && !p.has(player, "mcftpromotions.self"))) {
                                sender.sendMessage(ChatColor.GOLD + "You cannot change your own group!");
                                return false;
                            }
                            String oldgroup = PermissionsHandler.getGroup(worldname, puser);
                            String action = checkAction(newgroup, oldgroup);
                            String word = grammar.get(newgroup);
                            if (word == null) {
                                word = "a";
                            }
                            if (!action.equals("samegroup")) {
                                changeGroup(puser, newgroup, 0);
                                String group = PermissionsHandler.getGroup(worldname, puser);
                                String message = "";
                                String prefix = "";
                                if (settings.get("usePrefixes").equalsIgnoreCase("true")) {
                                    prefix = fixColor(PermissionsHandler.getPrefix(worldname, puser, group));
                                    message += prefix;
                                }
                                message += puser;
                                String note = "";
                                if (args.length > 2) {
                                    for (int i = 2; i < args.length; i++) {
                                        String s = args[i];
                                        if (note.length() > 0) {
                                            note += " ";
                                        }
                                        note += s;
                                    }
                                }
                                if (action.equals("default")) {
                                    message += ChatColor.WHITE + " is now " + word + " " + prefix + group + ChatColor.WHITE + "!";
                                    note(puser, group, note, sender, pname, "Set to");
                                } else if (action.equals("promotion")) {
                                    message += ChatColor.DARK_GREEN + " is now " + word + " " + prefix + group + ChatColor.DARK_GREEN + "! Congratulations!";
                                    note(puser, group, note, sender, pname, "Promoted to");
                                } else if (action.equals("demotion")) {
                                    message += ChatColor.RED + " is now " + word + " " + prefix + group + ChatColor.RED + "! Ouch!";
                                    note(puser, group, note, sender, pname, "Demoted to");
                                } else {
                                    message += ChatColor.WHITE + " is now " + word + " " + prefix + group + ChatColor.WHITE + "!";
                                    note(puser, group, note, sender, pname, "Set to");
                                }
                                getServer().broadcastMessage(fixColor(message));
                            } else {
                                String group = PermissionsHandler.getGroup(worldname, puser);
                                String prefix = "";
                                if (settings.get("usePrefixes").equalsIgnoreCase("true")) {
                                    prefix = fixColor(PermissionsHandler.getPrefix(worldname, puser, group));
                                }
                                sender.sendMessage(prefix + puser + ChatColor.RED + " is already " + word + " " + prefix + group + ChatColor.RED + "!");
                            }
                            return true;
                        } else {
                            sender.sendMessage("Permission denied. You are not allowed to set " + pname + " to " + command);
                        }
                    }
                }
            } else if (args.length < 1) {
                sender.sendMessage(ChatColor.GOLD + "Not enough arguments!");
                sender.sendMessage(ChatColor.RED + "/" + ChatColor.GRAY + "group name " + ChatColor.YELLOW + "Changes a users group.");
            }
        }
        return false;
    }

    /**
     * This checks if a package is loaded/exists, just as an easy way to check
     * if the plugin is loaded/exists.
     * 
     * @param packages
     *            The package to check
     * @return True if the plugin exists, otherwise false.
     * 
     * @since 1.0.5
     */
    private static boolean pluginExists(final String[] packages) {
        try {
            for (String packg : packages) {
                Class.forName(packg);
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Checks to make sure a permissions plugin has been loaded.
     * 
     * @return True if one is present, otherwise false.
     * @since 1.0.5
     */
    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(
                net.milkbowl.vault.permission.Permission.class);
        if (permissionProvider != null) {
            p = permissionProvider.getProvider();
        }
        return (p != null);
    }

    /**
     * Changes a user's group.
     * 
     * @param puser
     *            The user's name.
     * @param group
     *            The group name.
     * @param attempt
     *            0 to try twice, 1 to try once.
     */
    private void changeGroup(final String puser, final String group, final int attempt) {
        if (attempt > 1) {
            log("We still could not connect to the permissions plugin.", "severe");
            return;
        }
        String plugin = p.getName();
        String groupname = group.replace("_", " ");
        if (plugin == "bPermissions") {
            getServer().dispatchCommand(getServer().getConsoleSender(), "user " + puser);
            getServer().dispatchCommand(getServer().getConsoleSender(), "user setgroup " + groupname);
        } else if (plugin == "PermissionsBukkit") {
            getServer().dispatchCommand(getServer().getConsoleSender(), "permissions player setgroup " + puser + " " + groupname);
        } else if (plugin == "GroupManager") {
            getServer().dispatchCommand(getServer().getConsoleSender(), "manuadd " + puser + " " + groupname);
        } else if (plugin == "PermissionsEx") {
            getServer().dispatchCommand(getServer().getConsoleSender(), "pex user " + puser + " group set " + groupname);
        } else if (plugin == "zPermissions") {
            getServer().dispatchCommand(getServer().getConsoleSender(), "permissions player " + puser + " setgroup " + groupname);
        } else {
            log("Oh no! We were unable to connect to your permissions plugin. We'll try reconnecting then changing the user's rank.", "warning");
            if (!setupPermissions()) {
                getServer().getPluginManager().disablePlugin(this);
                return;
            }
            changeGroup(puser, groupname, attempt + 1);
        }
    }

    /**
     * Adds a rank change note to a player.
     * 
     * @param user
     *            User to add note to.
     * @param group
     *            Group user was moved to.
     * @param note
     *            Optional reason of promotion.
     * @param sender
     *            Command sender.
     * @param staff
     *            Staff member who promoted.
     * @param prefix
     *            Prefix of note (e.g. "Promoted to").
     */
    private void note(final String user, final String group, final String note, final CommandSender sender, String staff, final String prefix) {
        if (profiler != null) {
            if (!(sender instanceof Player)) {
                staff = "Console";
            }
            if (!note.isEmpty()) {
                Notes.addNote(user, prefix + " " + group + ". (" + note + ")", sender, staff, false);
            } else {
                Notes.addNote(user, prefix + " " + group + ".", sender, staff, false);
            }
        }
    }

    /**
     * Checks the action needed between two different groups (e.g. promotion or
     * demotion).
     * 
     * @param newgroup
     *            The user's new group.
     * @param oldgroup
     *            The user's old group.
     * @return default, demotion, promotion, or samegroup
     */
    private String checkAction(final String newgroup, final String oldgroup) {
        Integer newgroupid = null;
        Integer oldgroupid = null;
        String action = "default";
        for (String group : groups.keySet()) {
            if (group.equals(newgroup)) {
                newgroupid = Integer.parseInt(groups.get(newgroup));
            }
            if (group.equals(oldgroup)) {
                oldgroupid = Integer.parseInt(groups.get(oldgroup));
            }
        }
        if (oldgroupid == null || newgroupid == null) {
            action = "default";
        } else if (oldgroupid > newgroupid) {
            action = "demotion";
        } else if (oldgroupid < newgroupid) {
            action = "promotion";
        } else if (oldgroupid == newgroupid) {
            action = "samegroup";
        }
        if (action.equals("default")) {
            log("Please set up your group order, with valid integers for every group, in the group order settings file. ('plugins/McftPromotions/grouporder.txt')",
                    "warning");
        }
        return action;
    }

    /**
     * Unshifts a string array.
     * 
     * @param str
     *            First element of new array.
     * @param array
     *            Array to add at end of new array.
     * @return Unshifted array.
     */
    private String[] unshift(final String str, final String[] array) {
        String[] newarray = new String[array.length + 1];
        newarray[0] = str;
        for (Integer i = 0; i < array.length; i++) {
            newarray[i + 1] = array[i];
        }
        return newarray;
    }

    /**
     * This replaces all default color codes in text with Bukkit ChatColors.
     * 
     * @param s
     *            The string to fix
     * @return A string with Bukkit ChatColors
     * @since 1.0.0
     */
    public static String fixColor(String s) {
        s = s.replace("&4", ChatColor.DARK_RED + "");
        s = s.replace("&c", ChatColor.RED + "");
        s = s.replace("&6", ChatColor.GOLD + "");
        s = s.replace("&e", ChatColor.YELLOW + "");
        s = s.replace("&2", ChatColor.DARK_GREEN + "");
        s = s.replace("&a", ChatColor.GREEN + "");
        s = s.replace("&b", ChatColor.AQUA + "");
        s = s.replace("&3", ChatColor.DARK_AQUA + "");
        s = s.replace("&1", ChatColor.DARK_BLUE + "");
        s = s.replace("&9", ChatColor.BLUE + "");
        s = s.replace("&d", ChatColor.LIGHT_PURPLE + "");
        s = s.replace("&5", ChatColor.DARK_PURPLE + "");
        s = s.replace("&f", ChatColor.WHITE + "");
        s = s.replace("&7", ChatColor.GRAY + "");
        s = s.replace("&8", ChatColor.DARK_GRAY + "");
        s = s.replace("&0", ChatColor.BLACK + "");
        return s;
    }

    /**
     * Sends a message to the logger.
     * 
     * @param s
     *            The message to send
     * @param type
     *            The level (info, warning, severe)
     * @since 1.0.5
     */
    public static void log(final String s, final String type) {
        String message = "[McftPromotions] " + s;
        String t = type.toLowerCase();
        if (t != null) {
            boolean info = t.equals("info");
            boolean warning = t.equals("warning");
            boolean severe = t.equals("severe");
            if (info) {
                logger.info(message);
            } else if (warning) {
                logger.warning(message);
            } else if (severe) {
                logger.severe(message);
            } else {
                logger.info(message);
            }
        }
    }

    /**
     * Sends a message to the logger.
     * 
     * @param s
     *            The message to send.
     */
    public static void log(final String s) {
        log(s, "info");
    }
}
