package net.diamondmine.mcftpromotions;

import static net.diamondmine.mcftpromotions.McftPromotions.log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.HashMap;

/**
 * Handler for settings files.
 * 
 * @author Jon la Cour
 * @version 1.0.7
 */
public class Settings {
    public static final HashMap<String, String> commands = new HashMap<String, String>();
    public static final HashMap<String, String> grammar = new HashMap<String, String>();
    public static final HashMap<String, String> groups = new HashMap<String, String>();
    public static final HashMap<String, String> settings = new HashMap<String, String>();
    private static final String baseDir = "plugins/McftPromotions";
    private static final String commandsFile = "commands.txt";
    private static final String grammarFile = "grammar.txt";
    private static final String groupsFile = "grouporder.txt";
    private static final String settingsFile = "settings.txt";

    /**
     * This makes sure that the settings directory exists and creates the
     * default settings file if one is not present.
     * 
     * @since 1.0.0
     */
    public static void checkSettings() {
        String config = baseDir;
        String newline = System.getProperty("line.separator");
        File directory = new File(config);

        if (!directory.exists()) {
            if (directory.mkdir()) {
                log("Created directory '" + config + "'", "info");
            }
        }

        File commandsfile = new File(baseDir + "/" + commandsFile);
        File grammarfile = new File(baseDir + "/" + grammarFile);
        File groupsfile = new File(baseDir + "/" + groupsFile);
        File settingsfile = new File(baseDir + "/" + settingsFile);

        if (!commandsfile.exists()) {
            BufferedWriter f;
            try {
                f = new BufferedWriter(new FileWriter(baseDir + "/" + commandsFile));
                f.write("# Command = Group" + newline);
                f.write("# Command is the command used to promote/demote." + newline);
                f.write("# Group is the group that the command will promote/demote to." + newline);
                f.write("# Do not add a # before your lines. These are just comments/examples." + newline);
                f.write("# admin = Admin" + newline);
                f.close();
                log("Created commands setting file '" + baseDir + "/" + commandsFile + "'", "info");
            } catch (Exception e) {
                log("Unable to create commands setting file: " + e.getLocalizedMessage(), "warning");
            }
        }

        if (!grammarfile.exists()) {
            BufferedWriter f;
            try {
                f = new BufferedWriter(new FileWriter(baseDir + "/" + grammarFile));
                f.write("# Group = a" + newline);
                f.write("# This will allow you to have proper grammar in your promotion/demotion notices." + newline);
                f.write("# Group is the group that the grammar setting is for." + newline);
                f.write("# a is where you need to specify either a or an." + newline);
                f.write("# Do not add a # before your lines. These are just comments/examples." + newline);
                f.write("# The example below would produce 'Jim is now an Owner! Congratulations!'" + newline);
                f.write("# Owner = an" + newline);
                f.close();
                log("Created grammar setting file '" + baseDir + "/" + commandsFile + "'", "info");
            } catch (Exception e) {
                log("Unable to create grammar setting file: " + e.getLocalizedMessage(), "warning");
            }
        }
        if (!groupsfile.exists()) {
            BufferedWriter f;
            try {
                f = new BufferedWriter(new FileWriter(baseDir + "/" + groupsFile));
                f.write("# Group = #" + newline);
                f.write("# This is used to specify the order of ranks used in demotion/promotion messages." + newline);
                f.write("# Group is the group." + newline);
                f.write("# # is where you need to specify a number in the order of power." + newline);
                f.write("# Do not add a # before your lines. These are just comments/examples." + newline);
                f.write("# Guest = 1" + newline);
                f.write("# Member = 2" + newline);
                f.write("# Admin = 3" + newline);
                f.close();
                log("Created group order setting file '" + baseDir + "/" + commandsFile + "'", "info");
            } catch (Exception e) {
                log("Unable to create group order setting file: " + e.getLocalizedMessage(), "warning");
            }
        }
        if (!settingsfile.exists()) {
            BufferedWriter f;
            try {
                f = new BufferedWriter(new FileWriter(baseDir + "/" + settingsFile));
                f.write("# McftPromotions settings file" + newline);
                f.write("usePrefixes = true" + newline);
                f.close();
                log("Created settings file '" + baseDir + "/" + settingsFile + "'", "info");
            } catch (Exception e) {
                log("Unable to create settings file: " + e.getLocalizedMessage(), "warning");
            }
        }
    }

    /**
     * This loads all settings into a HashMap.
     * 
     * @since 1.0.0
     */
    public static void loadSettings() {
        String commandsfile = baseDir + "/" + commandsFile;
        String grammarfile = baseDir + "/" + grammarFile;
        String groupsfile = baseDir + "/" + groupsFile;
        String settingsfile = baseDir + "/" + settingsFile;
        String line;

        try {
            BufferedReader commandconfig = new BufferedReader(new FileReader(commandsfile));
            while ((line = commandconfig.readLine()) != null) {
                line = line.trim();
                if (!line.startsWith("#") && line.contains(" = ")) {
                    String[] pair = line.split(" = ", 2);
                    commands.put(pair[0], pair[1]);
                }
            }
            commandconfig.close();
            BufferedReader grammarconfig = new BufferedReader(new FileReader(grammarfile));
            while ((line = grammarconfig.readLine()) != null) {
                line = line.trim();
                if (!line.startsWith("#") && line.contains(" = ")) {
                    String[] pair = line.split(" = ", 2);
                    grammar.put(pair[0], pair[1]);
                }
            }
            grammarconfig.close();
            BufferedReader groupsconfig = new BufferedReader(new FileReader(groupsfile));
            while ((line = groupsconfig.readLine()) != null) {
                line = line.trim();
                if (!line.startsWith("#") && line.contains(" = ")) {
                    String[] pair = line.split(" = ", 2);
                    groups.put(pair[0], pair[1]);
                }
            }
            groupsconfig.close();
            BufferedReader settingsf = new BufferedReader(new FileReader(settingsfile));
            while ((line = settingsf.readLine()) != null) {
                line = line.trim();
                if (!line.startsWith("#") && line.contains(" = ")) {
                    String[] pair = line.split(" = ", 2);
                    Settings.settings.put(pair[0], pair[1]);
                }
            }
            settingsf.close();
        } catch (Exception e) {
            log("Unable to read setting files: " + e.getLocalizedMessage(), "severe");
        }
    }
}
